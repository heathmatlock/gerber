module Test.Data.Gerber.Command.FormatSpecification (run) where

import Data.Either (isLeft, isRight)
import Data.Function (($))
import System.IO (IO)
import Test.Hspec (Spec, describe, hspec, it)

import Data.Gerber.Command (mkFormatSpecification)

run :: IO ()
run = do
  hspec spec

spec :: Spec
spec = do
  describe "Format Specification" $ do
    integerDigits
    decimalDigits
  
integerDigits :: Spec
integerDigits = do
  it "fails if integer digits is less than 1" $
    isLeft (mkFormatSpecification 0 5)
  it "fails if integer digits is greater than 6" $
    isLeft (mkFormatSpecification 7 5)
  it "passes if integer digits is 1" $
    isRight (mkFormatSpecification 1 5)
  it "passes if integer digits is 6" $
    isRight (mkFormatSpecification 6 5)

decimalDigits :: Spec
decimalDigits = do
 it "fails if decimal digits is less than 5" $
   isLeft (mkFormatSpecification 2 4)
 it "fails if decimal digits greater than 6" $
   isLeft (mkFormatSpecification 2 7)
 it "passes if decimal digits digit is 5" $
   isRight (mkFormatSpecification 2 5)
 it "passes if decimal digits digit is 6" $
  isRight (mkFormatSpecification 2 6)
