import Control.Monad
import System.IO (IO)

import qualified Test.Data.Gerber.Command.FormatSpecification as FormatSpecificationSpec

main :: IO ()
main = do
  void FormatSpecificationSpec.run
