module Data.Gerber.Command.FormatSpecification (FormatSpecification, mkFormatSpecification) where

import Control.Applicative (pure)
import Data.Bool ((&&), (||), otherwise)
import Data.Either (Either(Left, Right))
import Data.Eq ((==), Eq)
import Data.Function (($))
import Data.Int (Int)
import Data.Ord ((>=), (<=), Ord)
import Data.Text (Text)
import Text.Show (Show)

{-
4.1 Format Specification (FS)
The FS (Format Specification) specifies the format of the coordinate data – see 3.6.3 - used in
operation commands. (The MO command specifies the unit in which the coordinates are
expressed, see 4.2.)

The FS command is mandatory. It must be used once and only once, in the header, before the
first use of coordinate data. It is recommended to put it as the very first non-comment line.

The syntax for the FS command is:
  <FS command> = FSLAX<Format>Y<Format>*
  <Format> = <digit><digit>

Syntax                     Comments
FS                         FS for Format Specification
LA                         These fixed characters are necessary for backwards compatibility.
                           See 7.3 for more details.
X<Format>Y<Format>         Specifies the format of X and Y coordinate data. The format of X
                           and Y coordinate must be the same; it is specified as X and Y
                           separately for backwards compatibility.
<Format> = <digit><digit>  The first digit sets the number of integer digits in the coordinate data,
                           the second the number of decimal digits. The number of integer digits can be is up to 6;
                           use the smallest number that fits the size of the image; 2 or 3 integer digits is typical.
                           The number of decimal digits must be 5 or 6.
Example:
  Syntax         Comments
  %FSLAX26Y26*%  Coordinates have 2 integer and 6 decimal digits maximum.

The resolution of a Gerber file is the distance expressed by the least significant digit of
coordinate data. The resolution of a Gerber file must be at least 0.001 mil or 25 nm. When the
unit is inch, the number of decimals must be set at 6; the resolution is then 0.001 mil or 25 nm.
When the unit is mm, the number of decimals must be at least 5, with a resolution of 10 nm, or
6, with a resolution of 1 nm. 
-}

-- NOTE: would be nice to restrict values at the type level.
data FormatSpecification
  = FormatSpecification IntegerDigits DecimalDigits deriving Show

mkFormatSpecification :: Int -> Int -> Either Text FormatSpecification
mkFormatSpecification i d = do
  possibleIntegerDigits <- mkIntegerDigits i
  possibleDecimalDigits <- mkDecimalDigits d
  pure $ FormatSpecification possibleIntegerDigits possibleDecimalDigits

newtype IntegerDigits = IntegerDigits { xFormat :: Int }
  deriving (Eq, Ord, Show)

mkIntegerDigits :: Int -> Either Text IntegerDigits
mkIntegerDigits n
  | n >= 1 && n <= 6 = Right (IntegerDigits n)
  | otherwise = Left "The upper bound on the number of integer digits is 6."

newtype DecimalDigits = DecimalDigits { yFormat :: Int }
  deriving (Eq, Ord, Show)

mkDecimalDigits :: Int -> Either Text DecimalDigits
mkDecimalDigits n
  | n == 5 || n == 6 = Right (DecimalDigits n)
  | otherwise = Left "The number of decimal digits must be 5 or 6."
