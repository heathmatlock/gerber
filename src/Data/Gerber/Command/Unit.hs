module Data.Gerber.Command.Unit (Unit(Inches, Millimeter)) where

import Data.Eq (Eq)
import Text.Show (Show)

{-
4.2 Unit (MO)
The MO (Mode) command sets the units used for coordinates and for parameters or modifiers
indicating sizes or coordinates. The units can be either inches or millimeters. This command is
mandatory. It must be used once and only once at the beginning of a file, before the first use of
coordinate data. Normally MO command follows immediately after FS command (see 4.1).

The syntax for the MO command is:
  <MO command> = MO(IN|MM)*

  Syntax   Comments

  MO       MO for Mode

  IN|MM    Units of the dimension data:
           IN – inches
           MM – millimeters

Examples:
  Syntax   Comments
  %MOIN*%  Dimensions are expressed in inches
  %MOMM*%  Dimensions are expressed in millimeters

Note: Always use mm, the metric unit, with 6 decimals. This expresses any imperial value
with a resolution of 1/100 of a mil or larger without any rounding – amply sufficient for PCBs. An
application may need to display imperial units, but that does not require the underlying file to be
imperial. Inch is only there for historic reasons and is now a useless embellishment.
-}

data Unit
  = Inches
  | Millimeter
  deriving (Eq, Show)
