module Data.Gerber.Command (module Export, Command(..)) where

import Data.Gerber.Command.FormatSpecification as Export
import Data.Gerber.Command.Unit as Export

data Command
  = FS  -- Format specification. Sets the coordinate format, e.g. the number of decimals. 4.1
  | MO  -- Mode. Sets the unit to inch or mm. 4.2
  | AD  -- Aperture define. Defines a template based aperture and assigns a D code to it. 4.3
  | AM  -- Aperture macro. Defines a macro aperture template. 4.5
  | AB  -- Aperture block. Defines a block aperture and assigns a D-code to it. 4.6
  | Dnn -- (nn≥10) Sets the current aperture to D code nn. 4.7
  | D01 -- Interpolate operation. Outside a region statement D01 creates a draw or arc object using the current aperture. Inside it creates a linear or circular contour segment. After the D01 command the current point is moved to draw/arc end point. 4.8
  | D02 -- Move operation. D02 does not create a graphics object but moves the current point to the coordinate in the D02 command. 4.8
  | D03 -- Flash operation. Creates a flash object with the current aperture. After the D03 command the current point is moved to the flash point. 4.8
  | G01 -- Sets the interpolation mode to linear. 4.9
  | G02 -- Sets the interpolation mode to clockwise circular. 4.10
  | G03 -- Sets the interpolation mode to counterclockwise circular. 4.10
  | G74 -- Sets quadrant mode to single quadrant. 4.10
  | G75 -- Sets quadrant mode to multi quadrant. 4.10
  | LP  -- Load polarity. Loads the polarity object transformation parameter. 4.11.2
  | LM  -- Load mirror. Loads the mirror object transformation parameter. 4.11.3
  | LR  -- Load rotation. Loads the rotation object transformation parameter. 4.11.4
  | LS  -- Load scale. Loads the scale object transformation parameter. 4.11.5
  | G36 -- Starts a region statement. This creates a region by defining its contour. 4.12.
  | G37 -- Ends the region statement. 4.12
  | SR  -- Step and repeat. Open or closes a step and repeat statement. 4.13
  | G04 -- Comment. 4.14
  | TF  -- Attribute file. Set a file attribute. 5.2
  | TA  -- Attribute aperture. Add an aperture attribute to the dictionary or modify it. 5.3
  | TO  -- Attribute object. Add an object attribute to the dictionary or modify it. 5.4
  | TD  -- Attribute delete. Delete one or all attributes in the dictionary. 5.5
  | M02 -- End of file. 4.15

