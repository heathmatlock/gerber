module Data.Gerber.GraphicsState (GraphicsState) where

import Data.Gerber.Command (Unit, FormatSpecification)

data GraphicsState =
  GraphicsState
    { coordinateFormat :: FormatSpecification
    , unit :: Unit
    }
    deriving Show
